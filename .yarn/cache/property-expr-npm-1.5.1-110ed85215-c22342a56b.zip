PK       !                node_modules/PK       !                node_modules/property-expr/PK
       ! ���v]  ]  '   node_modules/property-expr/package.json{
  "name": "property-expr",
  "version": "1.5.1",
  "description": "tiny util for getting and setting deep object props safely",
  "main": "index.js",
  "scripts": {
    "test": "node ./test.js",
    "debug": "node --inspect-brk ./test.js"
  },
  "repository": {
    "type": "git",
    "url": "https://github.com/jquense/expr/"
  },
  "keywords": [
    "expr",
    "expression",
    "setter",
    "getter",
    "deep",
    "property",
    "Justin-Beiber",
    "accessor"
  ],
  "author": "@monasticpanic Jason Quense",
  "license": "MIT",
  "prettier": {
    "singleQuote": true,
    "semi": false
  }
}
PK
       ! �����  �  )   node_modules/property-expr/.gitattributes# Auto detect text files and perform LF normalization
* text=auto

# Custom for Visual Studio
*.cs     diff=csharp
*.sln    merge=union
*.csproj merge=union
*.vbproj merge=union
*.fsproj merge=union
*.dbproj merge=union

# Standard to msysgit
*.doc	 diff=astextplain
*.DOC	 diff=astextplain
*.docx diff=astextplain
*.DOCX diff=astextplain
*.dot  diff=astextplain
*.DOT  diff=astextplain
*.pdf  diff=astextplain
*.PDF	 diff=astextplain
*.rtf	 diff=astextplain
*.RTF	 diff=astextplain
PK
       ! �J  J  #   node_modules/property-expr/index.js/**
 * Based on Kendo UI Core expression code <https://github.com/telerik/kendo-ui-core#license-information>
 */
'use strict'

function Cache(maxSize) {
  this._maxSize = maxSize
  this.clear()
}
Cache.prototype.clear = function() {
  this._size = 0
  this._values = {}
}
Cache.prototype.get = function(key) {
  return this._values[key]
}
Cache.prototype.set = function(key, value) {
  this._size >= this._maxSize && this.clear()
  if (!this._values.hasOwnProperty(key)) {
    this._size++
  }
  return this._values[key] = value
}

var SPLIT_REGEX = /[^.^\]^[]+|(?=\[\]|\.\.)/g,
  DIGIT_REGEX = /^\d+$/,
  LEAD_DIGIT_REGEX = /^\d/,
  SPEC_CHAR_REGEX = /[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g,
  CLEAN_QUOTES_REGEX = /^\s*(['"]?)(.*?)(\1)\s*$/,
  MAX_CACHE_SIZE = 512

var contentSecurityPolicy = false,
  pathCache = new Cache(MAX_CACHE_SIZE),
  setCache = new Cache(MAX_CACHE_SIZE),
  getCache = new Cache(MAX_CACHE_SIZE)

try {
  new Function('')
} catch (error) {
  contentSecurityPolicy = true
}

module.exports = {
  Cache: Cache,

  expr: expr,

  split: split,

  normalizePath: normalizePath,

  setter: contentSecurityPolicy
    ? function(path) {
      var parts = normalizePath(path)
      return function(data, value) {
        return setterFallback(parts, data, value)
      }
    }
    : function(path) {
      return setCache.get(path) || setCache.set(
        path,
        new Function(
          'data, value',
          expr(path, 'data') + ' = value'
        )
      )
    },

  getter: contentSecurityPolicy
    ? function(path, safe) {
      var parts = normalizePath(path)
      return function(data) {
        return getterFallback(parts, safe, data)
      }
    }
    : function(path, safe) {
      var key = path + '_' + safe
      return getCache.get(key) || getCache.set(
        key,
        new Function('data', 'return ' + expr(path, safe, 'data'))
      )
    },

  join: function(segments) {
    return segments.reduce(function(path, part) {
      return (
        path +
        (isQuoted(part) || DIGIT_REGEX.test(part)
          ? '[' + part + ']'
          : (path ? '.' : '') + part)
      )
    }, '')
  },

  forEach: function(path, cb, thisArg) {
    forEach(split(path), cb, thisArg)
  }
}

function setterFallback(parts, data, value) {
  var index = 0,
    len = parts.length
  while (index < len - 1) {
    data = data[parts[index++]]
  }
  data[parts[index]] = value
}

function getterFallback(parts, safe, data) {
  var index = 0,
    len = parts.length
  while (index < len) {
    if (data != null || !safe) {
      data = data[parts[index++]]
    } else {
      return
    }
  }
  return data
}

function normalizePath(path) {
  return pathCache.get(path) || pathCache.set(
    path,
    split(path).map(function(part) {
      return part.replace(CLEAN_QUOTES_REGEX, '$2')
    })
  )
}

function split(path) {
  return path.match(SPLIT_REGEX)
}

function expr(expression, safe, param) {
  expression = expression || ''

  if (typeof safe === 'string') {
    param = safe
    safe = false
  }

  param = param || 'data'

  if (expression && expression.charAt(0) !== '[') expression = '.' + expression

  return safe ? makeSafe(expression, param) : param + expression
}

function forEach(parts, iter, thisArg) {
  var len = parts.length,
    part,
    idx,
    isArray,
    isBracket

  for (idx = 0; idx < len; idx++) {
    part = parts[idx]

    if (part) {
      if (shouldBeQuoted(part)) {
        part = '"' + part + '"'
      }

      isBracket = isQuoted(part)
      isArray = !isBracket && /^\d+$/.test(part)

      iter.call(thisArg, part, isBracket, isArray, idx, parts)
    }
  }
}

function isQuoted(str) {
  return (
    typeof str === 'string' && str && ["'", '"'].indexOf(str.charAt(0)) !== -1
  )
}

function makeSafe(path, param) {
  var result = param,
    parts = split(path),
    isLast

  forEach(parts, function(part, isBracket, isArray, idx, parts) {
    isLast = idx === parts.length - 1

    part = isBracket || isArray ? '[' + part + ']' : '.' + part

    result += part + (!isLast ? ' || {})' : ')')
  })

  return new Array(parts.length + 1).join('(') + result
}

function hasLeadingNumber(part) {
  return part.match(LEAD_DIGIT_REGEX) && !part.match(DIGIT_REGEX)
}

function hasSpecialChars(part) {
  return SPEC_CHAR_REGEX.test(part)
}

function shouldBeQuoted(part) {
  return !isQuoted(part) && (hasLeadingNumber(part) || hasSpecialChars(part))
}
PK
       ! ���&6  6  &   node_modules/property-expr/LICENSE.txtThe MIT License (MIT)

Copyright (c) 2014 Jason Quense

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.PK
       ! -i�b�
  �
  $   node_modules/property-expr/README.mdexpr
=======

Tiny expression helper for creating compiled accessors; handles most stuff, including ["bracket notation"] for property access. Originally based off of Kendo UI Core expression code

    npm install property-expr

## Use

Setters and getters are compiled to functions and cached for Performance™

    var expr = require('property-expr')
      , obj = {
        foo: {
          bar: [ "hi", { buz: { baz: 'found me!' } }]
        }
      };

    var getBaz = expr.getter('foo.bar[1]["buz"].baz')
      , setBaz = expr.setter('foo.bar[1]["buz"].baz')

    console.log(getBaz(obj)) // => 'found me!'
    setBaz(obj, 'set me!')
    console.log(obj.foo.bar[1].buz.baz) // => 'set me!'

### `getter(expression, [ safeAccess ])`

Returns a function that accepts an obj and returns the value at the supplied expression. You can create a "safe" getter, which won't error out when accessing properties that don't exist, reducing existance checks befroe property access:

    expr.getter('foo.bar.baz', true)({ foo: {} }) // => undefined
    //instead of val = foo.bar && foo.bar.baz

### `setter(expression)`

Returns a function that accepts an obj and a value and sets the property pointed to by the expression to the supplied value.


### `expr(expression, [ safeAccess], [ paramName = 'data'])`

Returns a normalized expression string pointing to a property on root object
`paramName`.

    expr.expr("foo['bar'][0].baz", true, 'obj') // => "(((obj.foo || {})['bar'] || {})[0])"

### `split(path) -> Array`

Returns an array of each path segment.

```js
expr.split("foo['bar'][0].baz") // [ "foo", "'bar'", "0", "baz"]
```

### `forEach(path, iterator[, thisArg])`

Iterate through a path but segment, with some additional helpful metadata about the segment. The iterator function is called with: `pathSegment`, `isBracket`, `isArray`, `idx`, `segments`

```js
expr.forEach('foo["bar"][1]', function(pathSegment, isBracket, isArray, idx, segments) {
  // 'foo'   -> isBracket = false, isArray = false, idx = 0
  // '"bar"' -> isBracket = true,  isArray = false, idx = 1
  // '0'     -> isBracket = false, isArray = true,  idx = 2
})
```

### `normalizePath(path)`

Returns an array of path segments without quotes and spaces.
```js
expr.normalizePath('foo["bar"][ "1" ][2][ " sss " ]')
// ['foo', 'bar', '1', '2', ' sss ']
```

### `new Cache(maxSize)`

Just an utility class, returns an instance of cache. When the max size is exceeded, cache clears its storage.
```js
var cache = new Cache(2)
cache.set('a', 123) // returns 123
cache.get('a') // returns 123
cache.clear()

cache.set('a', 1)
cache.set('b', 2) // cache contains 2 values
cache.set('c', 3) // cache was cleaned automatically and contains 1 value
```
PK
       ! XӮ�E  E  "   node_modules/property-expr/test.jsvar a = require('assert'),
  expression = require('./index.js'),
  getter = expression.getter,
  setter = expression.setter,
  obj = {};

obj = {
  foo: {
    bar: ['baz', 'bux'],
    fux: 5,
    '00N40000002S5U0': 1,
    N40000002S5U0: 2,
    'FE43-D880-21AE': 3
  }
};

// -- Getters --
a.strictEqual(getter('foo.fux')(obj), 5);
a.deepEqual(getter('foo.bar')(obj), ['baz', 'bux']);

a.strictEqual(getter('foo.bar[1]')(obj), 'bux');
a.strictEqual(getter('["foo"]["bar"][1]')(obj), 'bux');
a.strictEqual(getter('[1]')([1, 'bux']), 'bux');

// safe access
a.strictEqual(getter('foo.fux', true)(obj), 5);
a.deepEqual(getter('foo.bar', true)(obj), ['baz', 'bux']);

a.strictEqual(getter('foo.bar[1]', true)(obj), 'bux');
a.strictEqual(getter('["foo"]["bar"][1]', true)(obj), 'bux');
a.strictEqual(getter('[1]', true)([1, 'bux']), 'bux');

a.strictEqual(getter('foo.gih.df[0]', true)(obj), undefined);
a.strictEqual(getter('["fr"]["bzr"][1]', true)(obj), undefined);

a.strictEqual(getter('foo["00N40000002S5U0"]', true)(obj), 1);
a.strictEqual(getter('foo.00N40000002S5U0', true)(obj), 1);
a.strictEqual(getter('foo["N40000002S5U0"]', true)(obj), 2);
a.strictEqual(getter('foo.N40000002S5U0', true)(obj), 2);
a.strictEqual(getter('foo["FE43-D880-21AE"]', true)(obj), 3);
a.strictEqual(getter('foo.FE43-D880-21AE', true)(obj), 3);

// -- Setters --
setter('foo.fux')(obj, 10);
a.strictEqual(obj.foo.fux, 10);

setter('foo.bar[1]')(obj, 'bot');
a.strictEqual(obj.foo.bar[1], 'bot');

setter('[\'foo\']["bar"][1]')(obj, 'baz');
a.strictEqual(obj.foo.bar[1], 'baz');

// -- Cache --
var cache = new expression.Cache(3)
a.strictEqual(cache._size, 0)
a.strictEqual(cache.set('a', a), a)
a.strictEqual(cache.get('a'), a)
a.strictEqual(cache._size, 1)
a.strictEqual(cache.set('b', 123), 123)
a.strictEqual(cache.get('b'), 123)
a.strictEqual(cache.set('b', 321), 321)
a.strictEqual(cache.get('b'), 321)
a.strictEqual(cache.set('c', null), null)
a.strictEqual(cache.get('c'), null)
a.strictEqual(cache._size, 3)
a.strictEqual(cache.set('d', 444), 444)
a.strictEqual(cache._size, 1)
cache.clear()
a.strictEqual(cache._size, 0)
a.strictEqual(cache.get('a'), undefined)

// -- split --

var parts = expression.split('foo.baz["bar"][1]');

a.strictEqual(parts.length, 4);

// -- join --

var parts = expression.split('foo.baz["bar"][1]');

a.strictEqual(expression.join(['0', 'baz', '"bar"', 1]), '[0].baz["bar"][1]');

a.strictEqual(expression.join(parts), 'foo.baz["bar"][1]');

// -- forEach --

var count = 0;

expression.forEach('foo.baz["bar"][1]', function(
  part,
  isBracket,
  isArray,
  idx
) {
  count = idx;

  switch (idx) {
    case 0:
      a.strictEqual(part, 'foo');
      a.strictEqual(isBracket, false);
      a.strictEqual(isArray, false);
      break;
    case 1:
      a.strictEqual(part, 'baz');
      a.strictEqual(isBracket, false);
      a.strictEqual(isArray, false);
      break;
    case 2:
      a.strictEqual(part, '"bar"');
      a.strictEqual(isBracket, true);
      a.strictEqual(isArray, false);
      break;
    case 3:
      a.strictEqual(part, '1');
      a.strictEqual(isBracket, false);
      a.strictEqual(isArray, true);
      break;
  }
});

a.strictEqual(count, 3);

// -- normalizePath --

a.deepEqual(
  expression.normalizePath('foo[ " bux\'s " ].bar["baz"][ 9 ][ \' s \' ]'),
  ['foo', ' bux\'s ', 'bar', 'baz', '9', ' s ']
)

console.log('--- Tests Passed ---');
PK       !             #   node_modules/property-expr/.vscode/PK
       ! ģJ�a  a  .   node_modules/property-expr/.vscode/launch.json{
  // Use IntelliSense to learn about possible attributes.
  // Hover to view descriptions of existing attributes.
  // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
  "version": "0.2.0",
  "configurations": [
    {
      "type": "node",
      "request": "launch",
      "name": "Launch test via NPM",
      "runtimeExecutable": "npm",
      "runtimeArgs": [
        "run-script",
        "debug"
      ],
      "port": 9229
    },
    {
      "type": "node",
      "request": "launch",
      "name": "Launch Program",
      "program": "${workspaceFolder}\\index.js"
    }
  ]
}PK?       !                        �A    node_modules/PK?       !                        �A+   node_modules/property-expr/PK?
       ! ���v]  ]  '           ��d   node_modules/property-expr/package.jsonPK?
       ! �����  �  )           ��  node_modules/property-expr/.gitattributesPK?
       ! �J  J  #           ��0  node_modules/property-expr/index.jsPK?
       ! ���&6  6  &           ���  node_modules/property-expr/LICENSE.txtPK?
       ! -i�b�
  �
  $           ��5  node_modules/property-expr/README.mdPK?
       ! XӮ�E  E  "           ��&  node_modules/property-expr/test.jsPK?       !             #           �A�3  node_modules/property-expr/.vscode/PK?
       ! ģJ�a  a  .           ���3  node_modules/property-expr/.vscode/launch.jsonPK    
 
 $  �6    